package vixer.service;

import vixer.repository.ProblemRepository;
import vixer.model.Problem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Sort.Direction;

@Service
public class StudentServiceImpl implements ProblemService {

    @Autowired
    private ProblemRepository dao;

    @Override
    public Page<Problem> findPaginated(int page, int size) {
        return dao.findAll(new PageRequest(page, size, Direction.DESC, "createdAt"));
    }
}