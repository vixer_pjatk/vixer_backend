package vixer.storage;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import org.apache.commons.io.FilenameUtils;

@Service
public class AzureStorageService implements StorageService {

    @Autowired
    private CloudStorageAccount storageAccount;

    @Override
    public String store(String containerName, MultipartFile file) {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        String contentType = file.getContentType();
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());

        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")) {
                // This is a security check
                throw new StorageException(
                        "Cannot store file with relative path outside current directory "
                                + filename);
            }

            CloudBlobClient serviceClient = storageAccount.createCloudBlobClient();

            // Container name must be lower case.
            CloudBlobContainer container = serviceClient.getContainerReference(containerName);
            if (!container.exists()) {
                container.createIfNotExists();
                BlobContainerPermissions containerPermissions = new BlobContainerPermissions();
                containerPermissions.setPublicAccess(BlobContainerPublicAccessType.CONTAINER);
                container.uploadPermissions(containerPermissions);
            }

            String uniqueFilename = java.util.UUID.randomUUID().toString() + "." + extension;
            CloudBlockBlob blob = container.getBlockBlobReference(uniqueFilename);

            while (blob.exists()) {
                uniqueFilename = java.util.UUID.randomUUID().toString() + "." + extension;
                blob = container.getBlockBlobReference(uniqueFilename);
            }

            blob.getProperties().setContentType(contentType);
            blob.upload(file.getInputStream(), file.getSize());

            return uniqueFilename;
        } catch (FileNotFoundException fileNotFoundException) {
            System.err.print("FileNotFoundException encountered: ");
            System.err.println(fileNotFoundException.getMessage());

            fileNotFoundException.printStackTrace();
        } catch (StorageException storageException) {
            System.err.print("StorageException encountered: ");
            System.err.println(storageException.getMessage());

            storageException.printStackTrace();
        } catch (Exception e) {
            System.err.print("Exception encountered: ");
            System.err.println(e.getMessage());

            e.printStackTrace();
        }

        return null;
    }
}
