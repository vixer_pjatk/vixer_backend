package vixer.storage;

import org.springframework.web.multipart.MultipartFile;

public interface StorageService {

    String store(String containerName, MultipartFile file);
}
