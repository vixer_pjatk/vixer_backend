package vixer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vixer.exception.ResourceNotFoundException;
import vixer.model.Problem;
import vixer.repository.ProblemRepository;
import vixer.storage.StorageService;
import org.springframework.data.domain.Page;

import javax.validation.Valid;
import java.util.List;
import vixer.service.ProblemService;

@RestController
@RequestMapping("/api")
public class ProblemController {

    @Autowired
    private ProblemRepository problemRepository;

    @Autowired
    private StorageService storageService;

    @Autowired
    private ProblemService service;

    @GetMapping(value ="/problems")
    public Page<Problem> findPaginatedDefault() {

        Page<Problem> resultPage = service.findPaginated(0, 10);

        return resultPage;
    }

    @GetMapping(value ="/problems", params ={"page"})
    public Page<Problem> findPaginated(@RequestParam(value ="page", defaultValue = "0", required=false) int page) {

        Page<Problem> resultPage = service.findPaginated(page, 10);

        return resultPage;
    }

    @PostMapping("/problems")
    public Problem createProblem(@RequestParam("imageFile") MultipartFile imageFile, @Valid @ModelAttribute Problem problem){

        String filename = storageService.store("problem", imageFile);
        problem.setImage(filename);

        return problemRepository.save(problem);
    }

    @GetMapping("/problems/{id}")
    public Problem getProblemById(@PathVariable(value = "id") Long problemId) {
        return problemRepository.findById(problemId)
                .orElseThrow(() -> new ResourceNotFoundException("Problem", "id", problemId));
    }

    @PutMapping("/problems/{id}")
    public Problem updateProblem(@PathVariable(value = "id") Long problemId,
                                           @Valid @RequestBody Problem problemDetails) {

        Problem problem = problemRepository.findById(problemId)
                .orElseThrow(() -> new ResourceNotFoundException("Problem", "id", problemId));

        problem.setTitle(problemDetails.getTitle());
        problem.setDescription(problemDetails.getDescription());

        Problem updatedProblem = problemRepository.save(problem);

        return updatedProblem;
    }
}
